from django.contrib import admin
from  collageapp.models import (register_table,Contact_Us,student,cors,Category,Add_Courses,cart,Order)

admin.site.site_header="Pioneer Python|Sandeep Kaur"

class Contact_UsAdmin(admin.ModelAdmin):
    
    fields =["contact_number","name","subject","message"]
    list_display =["id","name","contact_number","subject","message","added_on"]
    search_fields = ["name"]
    list_filter =["added_on","name"]
    list_editable =["message"]

class studentAdmin(admin.ModelAdmin):
    # fields =["name","email","roll_no"]
    list_display =["name","roll_no","email","fee","gender", "address"]
    search_fields =["roll_no", "name"]
    list_filter =["name", "gender"]
    list_editable =["email","address"]
     
class corsAdmin(admin.ModelAdmin):
    list_display = ["id","cor_name","description","added_on"]

class CategoryAdmin(admin.ModelAdmin):
    list_display = ["id","cat_name","description","added_on"]


admin.site.register(register_table)
admin.site.register(Contact_Us,Contact_UsAdmin)
admin.site.register(student,studentAdmin)
admin.site.register(cors,corsAdmin)
admin.site.register(Category,CategoryAdmin)
admin.site.register(Add_Courses)
admin.site.register(cart)
admin.site.register(Order)


