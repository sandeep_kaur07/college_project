from django.shortcuts import render,get_object_or_404,reverse
from django.http import HttpResponse,HttpResponseRedirect,JsonResponse
from collageapp.models import Contact_Us,student,register_table,cors,Category,Add_Courses,cart,Order
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from collageapp.forms import Add_Courses_form
from django.db.models import Q
from datetime import datetime 
from django.core.mail import EmailMessage

def col(request):
    return render(request,"base.html")

def wish(request):
    return render(request,"wish.html")

def purchase(request):
    return render(request,"studentpro.html")

def hom(request):
    return render(request,"home.html")

def con(request):
    all_data = Contact_Us.objects.all().order_by("-id")
    if request.method=="POST":
        nm = request.POST["name"]
        cont = request.POST["contact"]
        sub = request.POST["subject"]
        msz = request.POST["message"]
        
        data = Contact_Us(name=nm,contact_number=cont,subject=sub,message=msz)
        data.save()
        res = "Dear {} thanks for your feedback".format(nm)
        return render(request,"contectus.html",{"status":res,"messages":all_data})
        #return HttpResponse("<h1> Dear {} Data Saved Successful</h1>".format(nm))
    
    return render(request,"contectus.html",{"messages":all_data}) 
    return render(request,"contectus.html") 
     
def about(request):
    return render(request,"aboutus.html") 

def log(request):
    if request.method=="POST":
        un = request.POST["usernm"]
        pwd = request.POST["password1"]

        user = authenticate(username=un,password=pwd)
        if user:
            login(request,user)
            if user.is_superuser:
                return HttpResponseRedirect("/admin")
            else:
            
                res = HttpResponseRedirect("/instruction")
                if "remeberme" in request.POST:
                    res.set_cookie("user_id",user.id)
                    res.set_cookie("date_login",datetime.now())
                return res    
            #if user.is_active:
            #    return HttpResponseRedirect("/student")    
        else:
            return render(request,"home.html",{"status":"invaid username or password"})
        return HttpResponse("called") 
    return render(request,"login.html") 

def check_user(request):
    if request.method=="GET":
        un = request.GET["usern"]
        check = User.objects.filter(username=un)
        if len(check)==1:
            return HttpResponse("exists")
        else:    
            return HttpResponse("not exists")

def pas(request):
    context={}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    if request.method=="POST":
        current = request.POST["odpass"]
        new_pass = request.POST["nwpass"]
        
        user = User.objects.get(id=request.user.id)
        un = user.username
        pwd = new_pass
        check = user.check_password(current)
        if check==True:
            user.set_password(new_pass)
            user.save()
            
            authenticate(username=un,password=pwd)
            context["message"] = "password Change succcessfully"
            context["col"] = "alert-success "
            user = User.objects.get(username=un)
            login(request,user)
        else:
            context["message"] = "Incorrect Current Password"
            context["col"] = "alert-danger"
            
    return render(request,"reset.html",context)

@login_required
def user_logout(request):
    logout(request)
    res= HttpResponseRedirect("/") 
    res.delete_cookie("user_id")
    res.delete_cookie("date_login")
    return res
    

def dashboard(request):
    context = {}
    data = register_table.objects.get(user__id=request.user.id)
    context["data"]=data
    
    if "profile" in request.FILES:
            img = request.FILES["profile"]
            data.profile_pic = img
            data.save()
    return render(request,"dash_board.html")       

def all(request):
    context = {}
    #cats = Category.objects.all()
    all_course =Add_Courses.objects.all().order_by("course_name")
    context["course"] = all_course
    if "qury" in request.GET:
        q =request.GET["qury"]
       # p =request.GET["price"]
        cors = Add_Courses.objects.filter(course_name__icontains=q)
        #cors = Add_Courses.objects.filter(Q(course_name__icontains=p)&Q(sale_price__lt=p))
        #cors = Add_Courses.objects.filter(Q(course_name__icontains=q)&Q(course_category__cat_name__icontains=q))
        context["courses"]=cors
        context["abcd"]="search"

    return render(request,"all2.html",context)

@login_required
def instra(request):
    context = {}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"]=data
    
    if "profile" in request.FILES:
            img = request.FILES["profile"]
            data.profile_pic = img
            data.save()
    return render(request,"instr.html",context)

@login_required
def stu(request):
    context={}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    
    if "profile" in request.FILES:
            img = request.FILES["profile"]
            data.profile_pic = img
            data.save()
    return render(request,"studentpro.html",context)

def edtpro(request):
    context = {}
    check = register_table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"]=data
        
    if request.method=="POST":
    
        fn = request.POST["fname"]
        ln = request.POST["last"]
        em = request.POST["email"]      
        con = request.POST["contact"]
        age = request.POST["age"]
        city = request.POST["city"]
        gen = request.POST["gender"]
        occu = request.POST["occup"]
        about= request.POST["about"]
        data = register_table.objects.get(user__id=request.user.id)
        
        usr = User.objects.get(id=request.user.id)
        usr.first_name = fn
        usr.last_name = ln
        usr.email = em
        usr.save()

        data.contact_number = con
        data.age = age
        data.city = city
        data.gender = gen
        data.occupation = occu
        data. about = about
        data.save()
        
        if "profile" in request.FILES:
            img = request.FILES["profile"]
            data.profile_pic = img
            data.save()
        context["status"]="Change Saved Successfully"
    return render(request,"editpro.html",context)


def reg(request):
    if "user_id" in request.COOKIES:
        uid = request.COOKIES["user_id"]
        usr = get_object_or_404(User,id=uid)
        login(request,usr)
        if usr.is_superuser:
            return HttpResponseRedirect("/admin")
        if usr.is_active:
            return HttpResponseRedirect("/instruction")
    
    if request.method=="POST":
        fname = request.POST["first"]
        last = request.POST["last"]
        um = request.POST["uname"]
        pwd = request.POST["password1"]
        cpwd = request.POST["password2"]
        em = request.POST["email"]
        con = request.POST["contact"]
        
        tp = request.POST["utype"]
        print(request.POST)
        usr = User.objects.create_user(um,em,pwd)
        usr.first_name = fname
        usr.last_name = last
        if tp=="teach":
            usr.is_staff=True
        usr.save()

        reg = register_table(user=usr, contact_number=con)
        reg.save()

        return render(request,"regi.html",{"status":" register successfully".format(fname)})  
    return render(request,"regi.html") 

def add_Courses(request):
    context = {}
    check = register_table.objects.filter(user__id=request.user.id).order_by("-id")
    if len(check)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"]=data
    form = Add_Courses_form()  
    if request.method=="POST": 
        form = Add_Courses_form(request.POST,request.FILES)
        if form.is_valid():
            data = form.save(commit=False) 
            login_user = User.objects.get(username=request.user.username)
            data.instructor = login_user
            data.save()
            context["status"] = "{} addedd successfully".format(data.course_name)
    context["form"] = form
    return render(request,"addcourse.html",context)

def my_course(request):
    context = {}
    check = register_table.objects.filter(user__id=request.user.id).order_by("-id")
    if len(check)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"]=data
    #all = Add_Courses.objects.filter(instructor__id=request.user.id)
    all = Add_Courses.objects.all()
    context["course"] = all
    return render(request,"my_course.html",context)    

def single(request):
    context = {}
    pid = request.GET["pid"]
    obj = Add_Courses.objects.get(id=pid)
    context["course"] = obj
    return render(request,"sigl.html",context)

def update_course(request):
    context ={}
    cat = Category.objects.all().order_by("cat_name")
    context["category"] = cat
    
    pid = request.GET["pid"]
    course = Add_Courses.objects.get(id=pid)
    context["course"] = course
    if request.method=="POST":
        pn =request.POST["cname"]
        ct_id =request.POST["cocat"]
        cp =request.POST["cpri"]
        sp =request.POST["psale"]
        des =request.POST["des"]
        cat_obj = Category.objects.get(id=ct_id)
        course.course_name = pn
        course.course_category = cat_obj
        course.course_price = cp
        course.sale_price = sp
        course.course_detail= des
        if "pimg" in request.FILES:
            img = request.FILES["pimg"]
            course.course_image = img
        course.save()
        context["status"] = "Chnages are done!!"    
        context["id"] = pid    
    return render(request,"updatecourse.html",context)        

def delete_course(request):
    context= {}
    if "pid" in request.GET:
        pid = request.GET["pid"]
        cor = Add_Courses.objects.get(id=pid)
        context["course"]=cor
         
        if "action" in request.GET:
            cor.delete()
            context["status"] = str(cor.course_name)+"Deleted Successfully!!"
    return render(request,"deletecourse.html",context)

def sendmail(request):
    context = {}
    check = register_table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"]=data
    
    if request.method=="POST":
        res = request.POST["to"]  
        sub = request.POST["sub"]   
        msz = request.POST["msz"]  
        try:     
            em = EmailMessage(sub,msz,to=[res,])
            em.send()
            context["status"]= "Email Sent" 
            context["cls"]= "alert-success" 
        except:
            context["status"]= "Email Not Send ,please check internet connection/Email Address"
            context["cls"]= "alert-warning"  
    return render(request,"sending_mail.html",context)


def forgotpass(request):
    context = {}
    if request.method=="POST":
        un = request.POST["username"]
        pwd = request.POST["npass"]

        user = get_object_or_404(User,username=un)
        user.set_password(pwd)
        user.save()

        login(request,user)
        if user.is_superuser:
            return HttpResponseRedirect("/admin")
        else:
            return HttpResponseRedirect("/instruction")    
        # context["status"] = "Password Change Successfully!!!"
    return render(request,"Forget.html",context)

import random

def reset_pass(request):
    un = request.GET["username"]
    try:
        user = get_object_or_404(User,username=un) 
        otp = random.randint(1000,9999)
        msz = "Dear {} \n{} is your OTP \nDo not share with anyone \nThanku \nPIONEER PYTHON".format(user.username,otp)
        try:
            email = EmailMessage("Account Verification",msz,to=[user.email])
            email.send()
            return JsonResponse({"status":"sent","email":user.email,"rotp":otp})

        except:
            return JsonResponse({"status":"Error","email":user.email})
      
        
    except:
        return JsonResponse({"status":"failed"})
        
def carts(request):
    context={}
    items = cart.objects.filter(user__id=request.user.id,status=False)
    context["items"] = items
    if request.user.is_authenticated:
        if request.method=="POST":
            pid = request.POST["pid"]
            
            qty = request.POST["qty"]
            is_exist = cart.objects.filter(course__id=pid,user__id=request.user.id,status=False)
            if len(is_exist)>0:
                context["msz"] = "Item Already Exists in Your Cart"
                context["cls"] = "alert alert-warning"
            else:   
                course =get_object_or_404(Add_Courses,id=pid)
                usr = get_object_or_404(User,id=request.user.id)
                c = cart(user=usr,course=course,quantity=qty)
                c.save()
                context["msz"] = "{} Added in Your Cart".format(course.course_name)
                context["cls"] = "alert alert-success"
                
    else:
        context["status"] = "Please Login to View Your Cart"
    return render(request,"cart.html",context)


def get_cart_data(request):
    items = cart.objects.filter(user__id=request.user.id, status=False)
    sale,total,quantity =0,0,0
    for i in items:
        sale += float(i.course.sale_price)*i.quantity
        total += float(i.course.course_price)*i.quantity
        quantity+= int(i.quantity)

    res = {
        "total":total,"offer":sale,"quan":quantity,
    }    
    return JsonResponse(res)


def change_quant(request):
    if "quantity" in request.GET:
        pid = request.GET["pid"]
        qty = request.GET["quantity"]
        cart_obj = get_object_or_404(cart,id=pid)
        cart_obj.quantity = qty
        cart_obj.save()
        return HttpResponse(cart_obj.quantity)
    if "delete_cart" in request.GET:
        id = request.GET["delete_cart"]
        cart_obj = get_object_or_404(cart,id=id)
        cart_obj.delete()
        return HttpResponse(1)

from paypal.standard.forms import PayPalPaymentsForm
from django.conf import settings
def process_payment(request):
    items = cart.objects.filter(user__id=request.user.id,status=False)
    course=""
    amt=0
    inv = "INV-"+str(request.user.id)
    cart_ids = ""
    p_ids =""
    for j in items:
        course += str(j.course.course_name)+"\n"
        p_ids += str(j.course.id)+","
        amt += float(j.course.sale_price)
        inv += str(j.id)
        cart_ids += str(j.id)+","

    paypal_dict = {
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        'amount': str(amt),
                 
        'item_name': course,
        'invoice': inv,
        'notify_url': 'http://{}{}'.format("127.0.0.1:8000",
                                           reverse('paypal-ipn')),
        'return_url': 'http://{}{}'.format("127.0.0.1:8000",
                                           reverse('payment_done')),
        'cancel_return': 'http://{}{}'.format("127.0.0.1:8000",
                                              reverse('payment_cancelled')),
    }
    usr = User.objects.get(username=request.user.username)
    ord =Order(student_id=usr,cart_ids=cart_ids,course_ids=p_ids)
    ord.invoice_id= str(ord.id)+inv
    ord.save()
    request.session["order_id"]= ord.id

    form = PayPalPaymentsForm(initial=paypal_dict)
    return render(request, 'process_payment.html', {'form': form})
     
def payment_done(request):
    if "order id" in request.session:
        order_id = request.session["order_id"]
        ord_obj = get_object_or_404(Order,id=order_id)
        ord_obj.status=True
        ord_obj.save()
        
        for i in ord_obj.cart._ids.split(","):
            cart_object = cart.object.get(id=i)
            cart_object.status=True
            cart.object.save()
    return  render(request,"payment_success.html")

def payment_cancelled(request):
    return render(request,"payment_cencel.html")
    
def my_orders(request):
    context = {}
    check = register_table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"]=data

    all_orders= []
    orders = Order.objects.filter(student_id__id= request.user.id)
    for order in orders:
        course = []
        for id in order.course_ids.split(",")[:-1]:
            cor = get_object_or_404(Add_Courses,id=id)
            course.append(cor)
        ord = {
            "order_id":order.id,
            "course":course,
            "invoice": order.invoice_id,
            "status": order.status,
            "date":order.processed_on
        }        
        all_orders.append(ord)
    context["order_history"]= all_orders    
    return render(request,"order_history.html",context)    
      