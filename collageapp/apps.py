from django.apps import AppConfig


class CollageappConfig(AppConfig):
    name = 'collageapp'
