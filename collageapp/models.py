from django.db import models 
from django.contrib.auth.models import User
import datetime

class register_table(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    contact_number = models.IntegerField()
    profile_pic = models.ImageField(upload_to = "profile/%y/%m/%d",null=True)
    age = models.CharField(max_length=250,null=True)
    city = models.CharField(max_length=250,null=True)
    about = models.TextField(blank=True,null=True)
    gender = models.CharField(max_length=250,blank=True,null=True)
    occupation =models.CharField(max_length=250,null=True)
    added_on = models.DateTimeField(auto_now_add=True,null=True)
    update_on = models.DateTimeField(auto_now=True,null=True)
    
    def __str__(self):
        return self.user.username

class Contact_Us(models.Model):
    name = models.CharField(max_length=250)
    contact_number = models.IntegerField(blank=True)
    subject = models.CharField(max_length=250)
    message = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Contact_Us Table"

class student(models.Model):
    c =(
        ("M","Male"),("F","Female")
    )
    name = models.CharField(max_length=250)
    email = models.EmailField(max_length=200)
    roll_no = models.IntegerField(unique=True)
    fee = models.FloatField()
    gender = models.CharField(max_length=150, choices=c)
    address = models.TextField()
    is_registered = models.BooleanField()

    
    def __str__(self):
     return self.name+" "+str(self.roll_no)
    class Meta:
        verbose_name_plural = "Student Table"         


class cors(models.Model):
    cor_name = models.CharField(max_length=200)
    cover_pic = models.FileField(upload_to="media/%y/%m/%d")
    description = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)
                  
    def __str__(self):
        return self.cor_name

class Category(models.Model):
    cat_name = models.CharField(max_length=250)
    cover_pic = models.FileField(upload_to="category/%y/%m/%d")
    description = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.cat_name

    class Meta:
        verbose_name_plural = "Category Table" 

class Add_Courses(models.Model):
    instructor = models.ForeignKey(User,on_delete = models.CASCADE)
    instructor_name = models.CharField(max_length=250,null=True)
    course_name = models.CharField(max_length=250)
    course_category = models.ForeignKey(Category,on_delete = models.CASCADE )
    course_price = models.FloatField()
    sale_price = models.CharField(max_length=250)
    course_image = models.ImageField(upload_to="course_images/%y/%m/%d")
    course_details = models.TextField()

    def __str__(self):
        return self.course_name

    class Meta:
        verbose_name_plural = "Add_Course" 


class cart(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    course = models.ForeignKey(Add_Courses,on_delete=models.CASCADE)
    quantity = models.IntegerField()
    status = models.BooleanField(default=False)
    added_on = models.DateTimeField(auto_now_add=True,null=True)
    update_on = models.DateTimeField(auto_now_add=True,null=True)

    def __str__(self):
        return self.user.username

class Meta:
        verbose_name_plural = "Cart" 

class Order(models.Model):
    student_id = models.ForeignKey(User,on_delete=models.CASCADE)
    cart_ids = models.CharField(max_length=250)
    course_ids = models.CharField(max_length=250)
    invoice_id = models.CharField(max_length=250)
    status = models.BooleanField(default=False)
    processed_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.student_id.username        


    