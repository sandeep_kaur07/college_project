"""collageproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from collageapp import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path("h",views.col,name="head"),
    path("student",views.stu, name="profile"),
    path("",views.hom,name="home"),
    path("contect",views.con,name="contect"),
    path("about",views.about,name="aboutus"),
    path("wish",views.wish,name="wishlist"),
    path("purchse",views.purchase,name="purchaselist"),
    path("log",views.log,name="login"),
    path("registration",views.reg,name="reg"),
    path("check_user/",views.check_user,name="check_user"),
    path("coures",views.all,name="course"),
    path("Repass",views.pas,name="re"),
    path("singlecourse",views.single,name="single"),
    path("instruction",views.instra,name="instrau"),
    path("editprofile",views.edtpro,name="edit"),
    path("user_logout",views.user_logout,name="user_logout"),                     
    path("dashboard",views.dashboard,name="dashboard"),                               
    path("addcourses",views.add_Courses,name="addcourse"),
    path("mycourse",views.my_course,name="mycourse"),
    path("deletecourse",views.delete_course,name="deletecourse"),
    path("updatecourse",views.update_course,name="updatecourse"),
    path("sendingmail",views.sendmail,name="send_mail"),
    path("forgetting",views.forgotpass,name="forget"),
    path("reset",views.reset_pass,name="reset"),
    path("cart",views.carts,name="cart"),
    path("get_cart",views.get_cart_data,name="get_cart"),
    path("change_quant",views.change_quant,name="change_quant"),
    path("process_payment",views.process_payment,name="process_payment"),
    path("payment_done",views.payment_done,name="payment_done"),
    path("payment_cancelled",views.payment_cancelled,name="payment_cancelled"),
    path("my_orders",views.my_orders,name="my_orders"),

     path('paypal/', include('paypal.standard.ipn.urls')),
]+static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)