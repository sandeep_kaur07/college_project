ab = "This is a string in python"
st = "0123456789"
print(type(ab))
 #indexing
print(ab[-1]) # Forward indexing
print(ab[-1])
print(ab[-2]) #reverse indexing
#slicing
print(ab[0])
print(ab[2:])
print(ab[2:])
print(ab[-3:])
print(ab[-3:])
print(ab[-4:])

#step size
print(ab[::1])
print(ab[::2])
print(ab[::2])

